import threading
import time
import random
from threading import Thread, Semaphore, Lock

NUM_SAVAGES = 5
NUM_MISSIONARY = 10

# they all take food themselves
# if pot is empty, then savage must wake up the cook,
#   which must fill up the pot

pot = threading.Lock()
servings = 0
not_empty = threading.Semaphore(0)
empty = threading.Semaphore(0)


def eat(i: int):
    print(f"savage {i}: starts eating a portion")
    time.sleep(random.randint(1, 10) / 100)
    print(f"savage {i}: stops eating a portion")


def savage(i: int):
    global servings
    while True:
        pot.acquire()
        if servings == 0:
            print(f"JESSE WAKE UP WE HAVE TO COOK")
            empty.release()
        not_empty.acquire()
        servings -= 1
        print(f"Servings left {servings}")
        pot.release()
        eat(i)


def cooker(i: int):  # cooker je varic, cook je kuchar
    global servings
    while True:
        empty.acquire()
        time.sleep(3)
        servings += NUM_MISSIONARY # savage is holding the pot lock and waiting
        print(f"Mmmm {servings}")
        not_empty.release(NUM_MISSIONARY)


def run():
    savages = [Thread(target=savage, args=(i,)) for i in range(NUM_SAVAGES)]
    cookers = [Thread(target=cooker, args=(0,))]

    for t in savages + cookers:
        t.start()

    for t in savages + cookers:
        t.join()
