import time
from threading import Semaphore

import ChainReplication
import Chord
import CigaretteSmokers
import DistributedPhilosophers
import Gossip
import RWMutex
import Raft
import RicartAgrawala
import Savages
import SavagesDistributed
import SmokerValidator
import ThreadPool


def timed(runnable):
    start = time.time()
    runnable()
    end = time.time()
    print("Finished in " + str((end - start) * 1000) + "ms")


if __name__ == '__main__':
    # http://trnecka.inf.upol.cz/teaching/pds/cviceni01.html
    # timed(lambda: ThreadPool.run_threads_naive(8))

    # http://trnecka.inf.upol.cz/teaching/pds/cviceni02.html
    # timed(lambda: ThreadPool.run_thread_pool(8))  # 1
    # timed(lambda: ThreadPool.lamport_bakery())  # 2

    # http://trnecka.inf.upol.cz/teaching/pds/cviceni03.html
    # RWMutex.library() # 1
    # CigaretteSmokers.run() # 2
    # Savages.run() # 3
    # http://trnecka.inf.upol.cz/teaching/pds/cviceni04.html
    # DistributedPhilosophers.run()  # todo - works, but fork has no dirty state
    # http://trnecka.inf.upol.cz/teaching/pds/cviceni05.html
    # asyncio.run(SavagesDistributed.run())
    # http://trnecka.inf.upol.cz/teaching/pds/cviceni06.html
    # Chord.run()
    # http://trnecka.inf.upol.cz/teaching/pds/cviceni07.html
    # RicartAgrawala.run()
    # http://trnecka.inf.upol.cz/teaching/pds/cviceni08.html
    # Gossip.run()
    # http://trnecka.inf.upol.cz/teaching/pds/cviceni09.html
    # Raft.run()
    # http://trnecka.inf.upol.cz/teaching/pds/cviceni10.html
    # ChainReplication.run()
    pass
