s = """agent -1
paper +1
match +1
paper -1
lock -1
lock +1
match -1
lock -1
tobaccoSem +1
tobaccoSem -1
agent +1
lock +1
agent -1
paper +1
match +1
match -1
paper -1
lock -1
lock +1
lock -1
tobaccoSem +1
lock +1
tobaccoSem -1
agent +1
agent -1
tobacco +1
match +1
match -1
lock -1
lock +1
tobacco -1
lock -1
paperSem +1
lock +1
paperSem -1
agent +1
agent -1
tobacco +1
tobacco -1
lock -1
lock +1
paper +1
paper -1
lock -1
matchSem +1
matchSem -1
lock +1
agent +1
agent -1
paper +1
match +1
match -1
lock -1
lock +1
paper -1
lock -1
tobaccoSem +1
tobaccoSem -1
agent +1
agent -1
tobacco +1
lock +1
tobacco -1
match +1
lock -1
lock +1
match -1
lock -1
paperSem +1
lock +1"""

def validate():
    dict = {}
    for line in s.split("\n"):
        p = line.split(" ")
        v = dict.get(p[0], 0)
        dict[p[0]] = v + int(p[1])
    print(dict)
