import time
import random
from threading import Thread, Semaphore, Lock

agent = Semaphore(1)
tobacco = Semaphore(0)
paper = Semaphore(0)
match = Semaphore(0)

# stav ingredience - true = zablokovane k pouziti
isTobacco = False
isPaper = False
isMatch = False
tobaccoSem = Semaphore(0)
paperSem = Semaphore(0)
matchSem = Semaphore(0)
lock = Lock()

# def debug():
#     global isPaper, isMatch, isTobacco
#     print(f"Paper: {isPaper}, Match: {isMatch}, Tobacco: {isTobacco}")



def smoker_M(): # cekam na signal a pak kourim
    global agent, tobacco, paper, match, isTobacco, isPaper, isMatch, tobaccoSem, paperSem, matchSem, lock
    while True:
        print(f"Smoker Matches: Wants to make a cigarette")
        matchSem.acquire()
        #print("matchSem -1")
        print(f"Smoker Matches: Makes a cigarette")
        agent.release()
        #print("agent +1")
        print(f"Smoker Matches: Agent released")


def smoker_T():
    global agent, tobacco, paper, match, isTobacco, isPaper, isMatch, tobaccoSem, paperSem, matchSem, lock
    while True:
        print(f"Smoker Tobacco: Wants to make a cigarette")
        tobaccoSem.acquire()
        #print("tobaccoSem -1")
        print(f"Smoker Tobacco: Makes a cigarette")
        agent.release()
        #print("agent +1")
        print(f"Smoker Tobacco: Agent released")


def smoker_P():
    global agent, tobacco, paper, match, isTobacco, isPaper, isMatch, tobaccoSem, paperSem, matchSem, lock
    while True:
        print(f"Smoker Paper: Wants to make a cigarette")
        paperSem.acquire()
        #print("paperSem -1")
        print(f"Smoker Paper: Makes a cigarette")
        agent.release()
        #print("agent +1")
        print(f"Smoker Paper: Agent released")


def agent_A():  # hodi na stul tabak a papir
    global agent, tobacco, paper, match, isTobacco, isPaper, isMatch, tobaccoSem, paperSem, matchSem, lock
    while True:
        agent.acquire()
        #print("agent -1")
        print("Agent A: Throws tobacco and paper on table")
        tobacco.release()
        #print("tobacco +1")
        paper.release()
        #print("paper +1")
        print("Agent A: Throw complete")


def agent_B():
    global agent, tobacco, paper, match, isTobacco, isPaper, isMatch, tobaccoSem, paperSem, matchSem, lock
    while True:
        agent.acquire()
        #print("agent -1")
        print("Agent B: Throws matches and paper on table")
        paper.release()
        #print("paper +1")
        match.release()
        #print("match +1")
        print("Agent B: Throw complete")


def agent_C():
    global agent, tobacco, paper, match, isTobacco, isPaper, isMatch, tobaccoSem, paperSem, matchSem, lock
    while True:
        agent.acquire()
        #print("agent -1")
        print("Agent C: Throws tobacco and matches on table")
        tobacco.release()
        #print("tobacco +1")
        match.release()
        #print("match +1")
        print("Agent C: Throw complete")


def pusher_A():  # ukradne tabak a bud necha jineho pushera krast a nebo povoli kuraka
    global agent, tobacco, paper, match, isTobacco, isPaper, isMatch, tobaccoSem, paperSem, matchSem, lock

    while True:
        tobacco.acquire()  # čorka business
        #print("tobacco -1")
        lock.acquire()
        #print("lock -1")
        if isPaper:
            isPaper = False
            print("Pusher A: Match smoker is allowed to make cigarettes")
            matchSem.release()
            #print("matchSem +1")
        elif isMatch:
            isMatch = False
            print("Pusher A: Paper smoker is allowed to make cigarettes")
            paperSem.release()
            #print("paperSem +1")
        else:
            print("Pusher A: Tobacco is acquired")
            isTobacco = True
        lock.release()
        print("Lock released")
        #print("lock +1")


def pusher_B():
    global agent, tobacco, paper, match, isTobacco, isPaper, isMatch, tobaccoSem, paperSem, matchSem, lock
    while True:
        paper.acquire()
        #print("paper -1")
        lock.acquire()
        #print("lock -1")
        if isMatch:
            isMatch = False
            print("Pusher B: Tobacco smoker is allowed to make cigarettes")
            tobaccoSem.release()
            #print("tobaccoSem +1")
        elif isTobacco:
            isTobacco = False
            print("Pusher B: Match smoker is allowed to make cigarettes")
            matchSem.release()
            #print("matchSem +1")
        else:
            print("Pusher B: Paper is acquired")
            isPaper = True
        lock.release()
        print("Lock released")
        #print("lock +1")


def pusher_C():
    global agent, tobacco, paper, match, isTobacco, isPaper, isMatch, tobaccoSem, paperSem, matchSem, lock
    while True:
        match.acquire()
        #print("match -1")
        lock.acquire()
        #print("lock -1")
        if isTobacco:
            isTobacco = False
            print("Pusher C: Paper smoker is allowed to make cigarettes")
            paperSem.release()
            #print("paperSem +1")
        elif isPaper:
            isPaper = False
            print("Pusher C: Tobacco smoker is allowed to make cigarettes")
            tobaccoSem.release()
            #print("tobaccoSem +1")
        else:
            print("Pusher C: Match is acquired")
            isMatch = True
        lock.release()
        print("Lock released")
        #print("lock +1")


def run():
    global agent, tobacco, paper, match, isTobacco, isPaper, isMatch, tobaccoSem, paperSem, matchSem, lock
    threads = [
        Thread(target=agent_A),
        Thread(target=agent_B),
        Thread(target=agent_C),
        Thread(target=smoker_M),
        Thread(target=smoker_T),
        Thread(target=smoker_P),
        Thread(target=pusher_A),
        Thread(target=pusher_B),
        Thread(target=pusher_C)
    ]

    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()
