from threading import Lock, Thread
import time
import random


# fair depending on system mutex strategy - expected to be FIFO
class RWLock:
    def __init__(self) -> None:
        self.readers = 0
        self.read_lock = Lock()
        self.write_lock = Lock()
        self.count_lock = Lock()

    def acquire_new_reader(self) -> int:
        self.count_lock.acquire()
        current_readers = self.readers
        self.readers += 1
        self.count_lock.release()
        return current_readers

    def release_reader(self) -> int:
        self.count_lock.acquire()
        self.readers -= 1
        current_readers = self.readers
        self.count_lock.release()
        return current_readers

    def acquire_read(self) -> None:
        # print("Before acq write")
        self.write_lock.acquire()
        readers = self.acquire_new_reader()
        if readers == 0:
            # print("Before acq read")
            self.read_lock.acquire()
        # print("Before rel write")
        self.write_lock.release()

    def release_read(self) -> None:
        if self.release_reader() == 0:
            self.read_lock.release()

    def acquire_write(self) -> None:
        # print("Before write")
        self.write_lock.acquire()
        # print("Before read")
        self.read_lock.acquire()

    def release_write(self) -> None:
        self.read_lock.release()
        self.write_lock.release()


# použití
counter = 0
lock = RWLock()

WHITE = "\033[1;37;40m"
RED = "\033[1;31;40m"
GREEN = "\033[1;32;40m"
YELLOW = "\033[1;33;40m"


class User(Thread):
    def __init__(self, idx: int):
        super().__init__()
        self.idx = idx

    def run(self) -> None:
        while True:
            time.sleep(random.randrange(1, 10) / 100)
            lock.acquire_read()
            print(GREEN, f"User {self.idx} reading: {counter}")
            time.sleep(random.randrange(1, 10) / 100)
            lock.release_read()


class Librarian(Thread):
    def run(self) -> None:
        global counter
        while True:
            time.sleep(random.randrange(1, 10) / 100)
            print(WHITE, f"Librarian acquiring write")
            lock.acquire_write()
            counter += 1
            print(WHITE, f"Librarian writing new value: {counter}")
            time.sleep(random.randrange(1, 10) / 100)
            lock.release_write()


def library():
    threads = [
        User(0),
        User(1),
        User(2),
        User(3),
        User(4),
        Librarian()
    ]

    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()
