"""
	Inspirováno https://github.com/mikulatomas/distsim
	Simulace distribuovaného systému
"""
import typing
import random
from multiprocessing import Process, SimpleQueue
import time


def current_time_millis():
    return round(time.time() * 1000)


class Link:
    def __init__(self, node_1: 'Node', node_2: 'Node') -> None:
        self.node_1 = node_1
        self.node_2 = node_2


class Message:
    def __init__(self, by: str, to: str, msg: any) -> None:
        self.by = by
        self.to = to
        self.data = msg

    def __str__(self) -> str:
        return f"msg({self.by}, {self.to}, {self.data})"


class Node(Process):
    def __init__(self, id: str, target: typing.Callable, verbose: bool = True) -> None:
        super().__init__(name=id, target=target)
        self.connections_list = {}
        self.verbose = verbose

    def neighbors(self) -> typing.Tuple[str,]:
        return tuple(self.connections_list.keys())

    def recv_from(self, name: str) -> 'Message':
        q = self.connections_list[name]['incoming']
        if q.empty():
            msg = None
        else:
            msg = q.get()
            # self.log(f"recv {msg}")
            return msg

    def recv_from_block(self, name: str) -> 'Message':
        while True:
            q = self.connections_list[name]['incoming']
            if not q.empty():
                msg = q.get()
                # self.log(f"recv {msg}")
                return msg

    def recv_any(self) -> 'Message':
        neighbors_with_msg = [name for name in self.connections_list.keys() if not self.empty_incomming(name)]
        try:
            return self.recv_from(random.choice(neighbors_with_msg))
        except IndexError:
            return None

    def recv_any_block(self, timeout_ms: int = 0):
        now = current_time_millis()
        while timeout_ms == 0 or current_time_millis() - now > timeout_ms:
            msg = self.recv_any()
            if msg is not None:
                return msg
            else:
                time.sleep(0.005)

    def recv_block(self, node_from, timeout_ms=1_000_000):
        now = current_time_millis()
        while current_time_millis() - now < timeout_ms:
            msg = self.recv_from(node_from)
            if msg is not None:
                return msg
            else:
                time.sleep(0.005)

    def send_to(self, to: str, data: any) -> None:
        m = Message(self.name, to, data)
        self.connections_list[to]['outcoming'].put(m)
        # self.log(f"send {m}")

    def empty_outcomming(self, name: str) -> bool:
        q = self.connections_list[name]['outcoming']
        return True if q.empty() else False

    def empty_incomming(self, name: str) -> bool:
        q = self.connections_list[name]['incoming']
        return True if q.empty() else False

    def log(self, msg: str) -> None:
        if self.verbose:
            print(f"{self.name}: {msg}")


class Network:
    def __init__(self, nodes: typing.Collection['Node'], links: typing.Collection['Link'] = []) -> None:
        self.nodes = dict(((node.name, node) for node in nodes))

        for l in links:
            q1 = SimpleQueue()
            q2 = SimpleQueue()
            self.nodes[l.node_1].connections_list[l.node_2] = {'incoming': q1, 'outcoming': q2}
            self.nodes[l.node_2].connections_list[l.node_1] = {'incoming': q2, 'outcoming': q1}

    def start(self):
        for node in self.nodes.values():
            node.start()

    def terminate(self):
        for node in self.nodes.values():
            node.terminate()

    def kill(self):
        for node in self.nodes.values():
            node.kill()

    def join(self):
        for node in self.nodes.values():
            node.join()
