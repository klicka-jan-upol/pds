import asyncio

SAVAGES_COUNT = 5
FOOD_COUNT = 10


async def savage(i: int, q):
    while True:
        print(f"{i} is getting")
        task = await q.get()
        print(f"{i} is eating {task}")
        await asyncio.sleep(1)
        q.task_done()
        print(f"{i} is finished")


async def cook(q):
    total = 0
    while True:
        print("Cook is joining")
        await q.join()
        print("Cook is cooking")
        for i in range(FOOD_COUNT):
            print(f"Cooking {i}")
            await q.put(f"{total}")
            total += 1
            print(f"size {q.qsize()}")
        print("Cook finished")


async def run():
    q = asyncio.Queue()
    savages = [asyncio.create_task(savage(i, q)) for i in range(SAVAGES_COUNT)]
    cooks = [asyncio.create_task(cook(q))]
    all = savages + cooks
    await asyncio.gather(*all)
