import math
import random
import time
from queue import Queue

from lib.sds import Node, current_time_millis, Link, Network

ADD = "ADD"
REMOVE = "REMOVE"
PRINT = "PRINT"


class GossipGirl(Node):
    def __init__(self, id: int, period_ms, max_iterations, spread):
        super().__init__(str(id), self.loop)
        self.id = id
        self.period_ms = period_ms
        self.max_iterations = max_iterations
        self.spread = spread
        self.last_iteration_at_ms = current_time_millis()
        self.gossip_list = []
        self.spread_queue = None  # (type, number, iterations_remaining)

    def loop(self):
        self.spread_queue = Queue()
        while True:
            self.listen_to_other_gossip()
            if current_time_millis() > self.last_iteration_at_ms + self.period_ms:
                self.spread_gossip()
                self.last_iteration_at_ms = current_time_millis()
            time.sleep(0.01)

    def spread_gossip(self):
        while not self.spread_queue.empty():
            type, number, iterations = self.spread_queue.get_nowait()
            i = 0
            for node in random.choices(self.neighbors(), k=self.spread + 1):
                if node == "-1" or i >= self.spread:
                    continue
                self.log(f"Spreading gossip {type} {number} {iterations} to {node}")
                self.send_to(node, {"type": type, "number": number, "iterations": iterations})
                i += 1

    def listen_to_other_gossip(self):
        msg = self.recv_any()
        if msg is None:
            return
        type = msg.data['type']
        iterations = msg.data['iterations']
        number = msg.data['number']
        self.log(f"Received {type} {number} {iterations} from {msg.by}")
        if type != PRINT and iterations > 0:
            self.spread_queue.put_nowait((type, number, iterations - 1))
        if type == ADD and number not in self.gossip_list:
            self.gossip_list.append(number)
        if type == REMOVE and number in self.gossip_list:
            self.gossip_list.remove(number)
        if type == PRINT:
            self.print_gossips()

    def print_gossips(self):
        self.log(f"{self.gossip_list}")


class Client(GossipGirl):
    def __init__(self, id: int, period_ms, max_iterations, spread):
        super().__init__(id, period_ms, max_iterations, spread)

    def loop(self):
        entry = self.neighbors()[0]
        self.log(f"Spreading gossip ADD 5 {self.max_iterations} to {entry}")
        self.send_to(entry, {'type': ADD, 'number': 5, 'iterations': self.max_iterations})
        time.sleep(5)
        self.send_to(entry, {'type': ADD, 'number': 1, 'iterations': self.max_iterations})
        self.send_to(entry, {'type': ADD, 'number': 4, 'iterations': self.max_iterations})
        self.send_to(entry, {'type': REMOVE, 'number': 5, 'iterations': self.max_iterations})
        self.print()
        time.sleep(1)
        self.print()
        time.sleep(1)
        self.print()
        time.sleep(1)
        self.print()

    def print(self):
        for neighbor in self.neighbors():
            self.log(f"Sending PRINT to {neighbor}")
            self.send_to(neighbor, {'type': PRINT, 'number': 0, 'iterations': 0})


def run():
    no = 10
    spread = 3
    periods_ms = 1000
    max_iterations = (int(math.log(no, spread)) + 1) + 2
    ids = [i for i in range(no)]
    nodes = [GossipGirl(id, periods_ms, max_iterations, spread) for id in ids]
    client = Client(-1, periods_ms, max_iterations, spread)
    ids.append(-1)
    links = [Link(str(i), str(j)) for j in ids for i in ids if i != j] + [Link("-1", "0")]
    all = nodes + [client]

    network = Network(all, links)

    network.start()
    network.join()
