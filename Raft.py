import random
import time
from abc import abstractmethod, ABC

from lib.sds import Node, current_time_millis, Link, Network

LEADER = "LEADER"
FOLLOWER = "FOLLOWER"
CANDIDATE = "CANDIDATE"

ELECTION_START = "ELECTION_START"
ELECTION_YES = "ELECTION_YES"
LEADER_ELECTED = "LEADER_ELECTED"
HEART_BEAT = "HEART_BEAT"


class Data:
    def __init__(self, type, election_year, id):
        self.type = type
        self.election_year = election_year
        self.id = id

    def __repr__(self):
        return f"<Data {self.id}: {self.type} - {self.election_year}>"


class TimeGenerator:
    def __init__(self, lower, upper):
        self.lower = lower
        self.upper = upper

    def generate(self) -> int:
        return int(random.randint(self.lower, self.upper))


class RaftNode(Node):
    def __init__(self,
                 id: int,
                 election_period: TimeGenerator,
                 election_length: TimeGenerator,
                 heart_beat_rate,
                 other_nodes: []):
        super().__init__(str(id), self.loop)
        self.id = id
        self.other_nodes = other_nodes.copy()
        self.other_nodes.remove(self.name)
        self.needed_votes = len(other_nodes) / 2
        self.election_period = election_period
        self.election_length = election_length
        self.heart_beat_rate_ms = heart_beat_rate

        self.election_year = 0
        self.next_elections_at_ms = 0
        self.yes_received = 0
        # self.elections_reset_at_ms = 0

        self.give_up_leadership_at_ms = 0
        self.next_heartbeat_at_ms = 0
        self.last_heartbeat_at_ms = 0

        self.last_agreed_election_year = -1
        self.election_end_at_ms = 0

        self.leader = None
        self.state = FOLLOWER
        self.behaviour = FollowerBehaviour()

    def loop(self):
        while True:
            self.behaviour.handle_state(self)
            self.handle_incoming()
            time.sleep(0.05)

    def build_data(self, type):
        # return {'type': type, 'election_year': self.election_year, 'id': self.id}
        return Data(type, self.election_year, self.id)

    def handle_incoming(self):
        while True:
            msg = self.recv_any()
            if msg is None:
                break
            self.behaviour.handle_incoming(self, msg.by, msg.data)

    def become_leader(self):
        self.log(f"Telling others of new leader (me)")
        for x in self.other_nodes:
            self.send_to(x, self.build_data(LEADER_ELECTED))
        self.give_up_leadership_at_ms = (current_time_millis()
                                         + self.election_length.generate() * 5)
        self.last_heartbeat_at_ms = current_time_millis()
        self.next_elections_at_ms = 0
        self.yes_received = 0
        self.to_leader()

    def initiate_elections(self):
        if self.leader is not None:
            self.log(f"No heartbeat from leader {self.leader} in {current_time_millis() - self.last_heartbeat_at_ms}ms")
        self.log(f"Initiating election")
        self.election_year += 1
        self.yes_received = 1

        for x in self.other_nodes:
            self.send_to(x, self.build_data(ELECTION_START))

        self.to_candidate()
        self.next_elections_at_ms = 0
        self.election_end_at_ms = current_time_millis() + self.election_length.generate()

    def sync_election_year(self, election_year):
        if election_year > self.election_year:
            self.election_year = election_year

    def agree_with_elections(self, by, data):
        self.sync_election_year(data.election_year)
        self.log(f"Agreeing to {data}")
        self.send_to(by, self.build_data(ELECTION_YES))
        self.last_agreed_election_year = data.election_year
        self.sync_election_year(data.election_year)
        self.election_end_at_ms = current_time_millis() + self.election_length.generate()
        self.next_elections_at_ms = 0
        self.to_follower(None)

    def accept_new_leader(self, by, election_year):
        self.sync_election_year(election_year)
        self.log(f"Following new leader {by}")
        self.leader = by
        self.yes_received = 0
        self.election_end_at_ms = 0
        self.next_elections_at_ms = 0
        self.last_heartbeat_at_ms = current_time_millis()
        self.to_follower(by)

    def choose_next_election_start(self):
        self.next_elections_at_ms = current_time_millis() + self.election_period.generate()
        self.election_end_at_ms = 0
        self.log(f"Next elections at {self.next_elections_at_ms}")

    def accept_new_vote(self, by):
        self.yes_received += 1
        self.log(f"New vote from {by} - total {self.yes_received}")
        if self.yes_received > self.needed_votes:
            self.become_leader()

    def cancel_election_run(self):
        self.log(f"Cancelling my election run")
        self.to_follower(None)
        self.election_end_at_ms = 0
        self.next_elections_at_ms = 0

    def give_up_leadership(self):
        self.log(f"Giving up leadership")
        # has to be fair to allow other nodes to stop waiting for heart beat
        while current_time_millis() > self.last_heartbeat_at_ms + self.heart_beat_rate_ms + 25:
            pass
        self.give_up_leadership_at_ms = 0
        self.last_heartbeat_at_ms = 0
        self.next_elections_at_ms = 0
        self.to_follower(None)

    def send_heartbeat(self):
        self.log(f"Sending heartbeat")
        self.last_heartbeat_at_ms = current_time_millis()
        for x in self.other_nodes:
            self.send_to(x, self.build_data(HEART_BEAT))

    def accept_heart_beat(self):
        self.last_heartbeat_at_ms = current_time_millis()

    def to_follower(self, new_leader):
        self.log(f"Becoming follower")
        self.state = FOLLOWER
        self.behaviour = FollowerBehaviour()
        self.leader = new_leader

    def to_candidate(self):
        self.log(f"Becoming candidate")
        self.state = CANDIDATE
        self.behaviour = CandidateBehaviour()

    def to_leader(self):
        self.log(f"Becoming leader")
        self.leader = self.name
        self.state = LEADER
        self.behaviour = LeaderBehaviour()


class NodeBehaviour(ABC):
    def __init__(self):
        pass

    @abstractmethod
    def handle_incoming(self, node: RaftNode, by: str, data: Data):
        pass

    @abstractmethod
    def handle_state(self, node: RaftNode):
        pass


class FollowerBehaviour(NodeBehaviour):

    def __init__(self):
        super().__init__()

    def handle_incoming(self, node: RaftNode, by: str, data: Data):
        if data.type == ELECTION_START:
            if node.last_agreed_election_year < data.election_year:
                node.agree_with_elections(by, data)
            else:
                node.log(f"Refused {data}- year:{node.election_year}, {node.last_agreed_election_year}")
        elif data.type == LEADER_ELECTED:
            node.accept_new_leader(by, data.election_year)
        elif data.type == HEART_BEAT:
            if by != node.leader:
                node.log(f"Wrong leader heartbeat! Current {node.leader} vs fake leader {by}")
            else:
                node.accept_heart_beat()

    def handle_state(self, node: RaftNode):
        if node.next_elections_at_ms == 0:
            # nobody is running for a candidate
            # or somebody ran for a candidate, but it was long time ago
            if (node.last_agreed_election_year < node.election_year
                    or current_time_millis() > node.election_end_at_ms):
                node.choose_next_election_start()
        elif (node.leader is None
              and current_time_millis() > node.next_elections_at_ms
        ) or (node.leader is not None
              and current_time_millis() > node.last_heartbeat_at_ms + node.heart_beat_rate_ms):
            node.log(f"curr {current_time_millis() % 10000} "
                     f"vs next el {node.next_elections_at_ms % 10000}"
                     f" vs end {node.election_end_at_ms % 10000}")
            node.initiate_elections()


class CandidateBehaviour(NodeBehaviour):
    def handle_incoming(self, node: RaftNode, by: str, data: Data):
        if data.type == ELECTION_YES:
            node.accept_new_vote(by)
        elif data.type == LEADER_ELECTED:  # and data.election_year >= node.election_year:
            node.accept_new_leader(by, data.election_year)
        elif data.type == ELECTION_START and data.election_year > node.election_year:
            node.agree_with_elections(by, data)

    def handle_state(self, node: RaftNode):
        if current_time_millis() > node.election_end_at_ms:
            node.cancel_election_run()


class LeaderBehaviour(NodeBehaviour):
    def handle_incoming(self, node: RaftNode, by: str, data: Data):
        pass

    def handle_state(self, node: RaftNode):
        if current_time_millis() > node.give_up_leadership_at_ms:
            node.give_up_leadership()
        elif current_time_millis() > node.last_heartbeat_at_ms + node.heart_beat_rate_ms / 3 * 2:
            node.send_heartbeat()


election_period_leader = TimeGenerator(150, 151)
election_period_follower = TimeGenerator(1500, 3000)

election_period_node = TimeGenerator(600, 1000)
election_length_node = TimeGenerator(1500, 1500)
heart_beat = 750


def single_leader_election(names, ids):
    nodes = [RaftNode(id, election_period_node, election_length_node, heart_beat, names) for id in ids if id != 0]
    nodes.append(RaftNode(0, election_period_leader, election_length_node, heart_beat, names))
    return nodes


def double_leader_election(names, ids):
    nodes = [RaftNode(id, election_period_node, election_length_node, heart_beat, names) for id in ids if
             id != 0 and id != 1]
    nodes.append(RaftNode(0, election_period_leader, election_length_node, heart_beat, names))
    nodes.append(RaftNode(1, election_period_leader, election_length_node, heart_beat, names))
    return nodes


def equal_election(names, ids):
    return [RaftNode(id, election_period_node, election_length_node, heart_beat, names) for id in ids]


def run():
    no = 6
    ids = [i for i in range(no)]
    names = [str(i) for i in ids]
    # nodes = single_leader_election(names, ids)
    # nodes = double_leader_election(names, ids)
    nodes = equal_election(names, ids)
    links = [Link(str(i), str(j)) for j in ids for i in ids if i != j]

    network = Network(nodes, links)

    network.start()
    network.join()
