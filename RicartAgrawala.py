import multiprocessing
import random
import sys
import time
from queue import PriorityQueue

from lib.sds import Node, current_time_millis, Link, Network

LOCK = 'LOCK'
UNLOCK = 'UNLOCK'
REQUEST_ACCESS = 'REQUEST_ACCESS'
ACKNOWLEDGED = 'ACKNOWLEDGED'
SYNC = 'SYNC'

SYNC_INTERVAL = 1000 * 5


class MutexNode(Node):
    def __init__(self, id: int, event):
        super().__init__(f"{id}", self.loop)
        self.id = id
        self.allocated_to = None
        self.event = event

    def loop(self):
        while True:
            if not self.event.is_set():
                self.log("exit")
                return
            msg = self.recv_any_block(timeout_ms=1000)
            if msg is not None:
                if msg.data['type'] == LOCK:
                    if self.allocated_to is not None:
                        self.log(
                            f"{msg.by} is trying to enter critical section already allocated to {self.allocated_to}")
                        self.event.clear()
                    else:
                        self.allocated_to = msg.by
                        self.log(f"{msg.by} successfully entered critical section")
                elif msg.data['type'] == UNLOCK:
                    if self.allocated_to is not None:
                        if self.allocated_to == msg.by:
                            self.log(f"{msg.by} successfully left critical section")
                            self.allocated_to = None
                        else:
                            self.log(f"{msg.by} is trying to leave critical section allocated to {self.allocated_to}")
                            self.event.clear()
                    else:
                        self.log(f"{msg.by} is trying to leave critical section that isn't allocated to anyone")
                        self.event.clear()


class RANode(Node):
    def __init__(self, id: int, ra_nodes: [], resource, event):
        super().__init__(f"{id}", self.loop)
        self.id = id
        self.ra_nodes = ra_nodes
        self.resource = resource

        self.ack_queue = None
        self.clock = 0

        self.requested_access = False
        self.is_inside_critical_section = False

        self.wants_next_access_at_ms = 0
        self.requested_at_ms = 0
        self.requested_at_clock = 0
        self.received_ack_yes = 0

        self.leave_critical_section_at = 0

        self.last_sync_at = current_time_millis()
        self.quit_process_event = event
        self.print_debug = True

    def inc_clock(self):
        self.clock += 1

    def loop(self):
        self.ack_queue = PriorityQueue()
        self.wants_next_access_at_ms = current_time_millis() + (random.random() + 0.2) * 1000
        while True:
            if not self.quit_process_event.is_set():
                self.log("exit")
                return
            if self.is_inside_critical_section:
                self.try_unlock()
                pass
            if not self.requested_access and current_time_millis() > self.wants_next_access_at_ms:
                self.request_access()
            self.ack()
            if self.print_debug and current_time_millis() > self.last_sync_at + SYNC_INTERVAL:
                self.last_sync_at = current_time_millis()
                self.debug()
                # self.sync()
            time.sleep(0.003)

    def debug(self):
        items = []
        text = ""
        while True:
            try:
                item = self.ack_queue.get_nowait()
            except:
                break
            text = text + str(item) + ","
            items += [item]
        for item in items:
            self.ack_queue.put_nowait(item)
        # self.sync = False
        self.log(f"Node {self.id} ----------------------------------\n"
                 f"clock: {self.clock}, ack_count: {self.received_ack_yes}\n"
                 f"requested_access {self.requested_access}, is_inside {self.is_inside_critical_section}\n"
                 f"queue: {text}")

    # def sync(self):
    #     for neighbor in self.neighbors():
    #
    #         if neighbor != self.resource:
    #             self.send_to(neighbor, {'req_clock': self.clock, 'type': SYNC})

    def request_access(self):
        self.log(f"Requesting access")
        self.requested_access = True
        self.inc_clock()
        self.requested_at_ms = current_time_millis()
        self.requested_at_clock = self.clock
        for node_id in self.ra_nodes:
            if node_id != self.id:
                self.send_to(str(node_id), {'req_clock': self.clock, 'type': REQUEST_ACCESS})

    def ack(self):
        while True:
            msg = self.recv_any()
            if msg is None:
                return
            type = msg.data['type']
            req_clock = msg.data['req_clock']
            self.sync_clock(req_clock)
            by = msg.by
            self.log(f"Handling {msg}")
            if type == ACKNOWLEDGED:
                if req_clock == self.requested_at_clock:
                    self.received_ack_yes += 1
                    self.log(f"Received ACK {self.received_ack_yes}")
                    self.try_enter_critical_section()
                else:
                    self.log(f"Incorrect clock ACK")
                    self.quit_process_event.clear()
            elif type == REQUEST_ACCESS:
                # ack yes
                if (not self.is_inside_critical_section
                        and (not self.requested_access or req_clock < self.requested_at_clock)):
                    self.log(f"Acknowledging request {req_clock} {by}")
                    self.inc_clock()
                    self.send_to(by, {'req_clock': req_clock, 'type': ACKNOWLEDGED})
                else:
                    self.log(f"Enqueing request {req_clock} {by}")
                    self.ack_queue.put_nowait((req_clock, int(by)))
            # elif type == SYNC:
            #     self.log(f"sync {self.clock} and {req_clock}")
            #     self.sync_clock(req_clock)

    def sync_clock(self, req_clock):
        if self.clock < req_clock:
            self.clock = req_clock

    def try_enter_critical_section(self):
        if self.received_ack_yes == len(self.ra_nodes) - 1:
            self.requested_at_ms = 0
            self.requested_at_clock = 0
            self.log(f"Accessing critical section at {self.clock}")
            self.received_ack_yes = 0
            self.is_inside_critical_section = True
            self.send_to(self.resource, {'req_clock': self.clock, 'type': LOCK})
            hold_access_for = int((random.random() + 0.333) * 3_000)  # lock for 1 - 4 s
            self.log(f"Accessing critical section at {self.clock} for {hold_access_for}ms")
            self.leave_critical_section_at = current_time_millis() + hold_access_for

    def try_unlock(self):
        if current_time_millis() > self.leave_critical_section_at:
            self.log(f"Unlocking critical section at {self.clock}")
            self.send_to(self.resource, {'req_clock': self.clock, 'type': UNLOCK})
            self.leave_critical_section_at = 0
            self.requested_access = False
            self.is_inside_critical_section = False
            delay = (random.random() + 0.2) * 1000  # 0.2 - 1.2 s
            self.wants_next_access_at_ms = current_time_millis() + delay
            self.received_ack_yes = 0

            while True:
                try:
                    clock, who = self.ack_queue.get_nowait()
                    if clock > self.clock: self.clock = clock
                    self.log(f"acknowledging other request at {clock} by {who}")
                    self.send_to(str(who), {'req_clock': clock, 'type': ACKNOWLEDGED})
                except:
                    break


def run():
    event = multiprocessing.Manager().Event()
    event.set()
    resource = MutexNode(-1, event)
    no = 5
    ids = [i for i in range(no)]
    nodes = [RANode(id, ids, resource.name, event) for id in range(no)]

    all = nodes + [resource]
    names = [x.name for x in all]
    links = [Link(i, j) for j in names for i in names if i != j]

    network = Network(all, links)

    network.start()
    network.join()
