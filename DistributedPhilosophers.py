import random
import typing
from multiprocessing import current_process
from lib.sds import Link, Node, Network

AVAILABLE_MSG = "Give me fork?"
YES_MSG = "Yes."
NO_MSG = "No."
FREE_MSG = "Dobby is free."
GIVE_UP_LEFT = "Resistence is futile."
GIVE_UP_RIGHT = "Resistence is even more."


def fork():
    available = True
    node = current_process()
    while True:
        msg = node.recv_any_block()
        if msg is not None:
            if msg.data == AVAILABLE_MSG:
                if available:
                    node.send_to(msg.by, YES_MSG)
                    available = False
                else:
                    node.send_to(msg.by, NO_MSG)
            elif msg.data == FREE_MSG:
                available = True


class Philosopher(Node):
    def __init__(self, id: str) -> None:
        super().__init__(id, self.big_brain)
        self.has_left = False
        self.has_right = False
        self.left_node = None
        self.right_node = None
        self.left_fork = None
        self.right_fork = None

    def try_acquire(self, target):
        self.send_to(target, AVAILABLE_MSG)
        response = self.recv_from_block(target)
        ret = response.data == YES_MSG
        if ret:
            print(f"{self.name} acquired {target}")
        return ret

    def eat(self):
        print(f"{self.name} is eating")
        self.send_to(self.left_fork, FREE_MSG)
        self.send_to(self.right_fork, FREE_MSG)

    def give_up_fork(self):
        msg = self.recv_any()
        if msg is not None:
            if msg.data == GIVE_UP_LEFT:
                if self.has_left:
                    self.has_left = False
                    self.log(f"Giving up {self.left_fork} to {msg.by}")
                    self.send_to(msg.by, YES_MSG)
                    return True
                else:
                    self.send_to(msg.by, NO_MSG)
            elif msg.data == GIVE_UP_RIGHT:
                if self.has_right:
                    self.has_right = False
                    self.log(f"Giving up {self.right_fork} to {msg.by}")
                    self.send_to(msg.by, YES_MSG)
                    return True
                else:
                    self.send_to(msg.by, NO_MSG)
        return False

    def try_request(self, node, msg):
        self.send_to(node, msg)
        response = self.recv_block(node, 100)
        return response is not None and response.data == YES_MSG

    def big_brain(self):
        while True:
            if not self.has_left:
                self.has_left = self.try_acquire(self.left_fork)
            if not self.has_right:
                self.has_right = self.try_acquire(self.right_fork)
            if self.has_right and self.has_left:
                self.eat()
                self.has_left = self.has_right = False
            if not self.give_up_fork() and (self.has_right or self.has_left):
                if self.has_left:
                    self.has_right = self.try_request(self.right_node, GIVE_UP_LEFT)
                    if self.has_right:
                        self.eat()
                        self.has_left = self.has_right = False
                elif self.has_right:
                    self.has_left = self.try_request(self.left_node, GIVE_UP_RIGHT)
                    if self.has_left:
                        self.eat()
                        self.has_left = self.has_right = False


def link(node, right_node, right_fork, left_fork, links):
    links.append(Link(node.name, right_node.name))
    links.append(Link(node.name, right_fork.name))
    links.append(Link(node.name, left_fork.name))
    node.right_node = right_node.name
    right_node.left_node = node.name
    node.left_fork = left_fork.name
    node.right_fork = right_fork.name
    # left_fork.right_node = node.name
    # right_fork.left_node = node.name


def run():
    no = 6
    philosophers = [Philosopher(f"Filozof {i}") for i in range(0, no)]
    forks = [Node(f"Fork {i}", fork) for i in range(0, no)]
    links = []
    for i in range(0, no):
        link(philosophers[i], philosophers[(i + 1) % no], forks[i], forks[(i - 1 + no) % no], links)

    network = Network(philosophers + forks, links)

    network.start()
    network.join()
