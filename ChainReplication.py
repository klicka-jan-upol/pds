import time

from lib.sds import Node, Network, Link, current_time_millis

WRITE = "WRITE"
WRITE_DONE = "WRITE_DONE"
READ = "READ"
READ_DONE = "READ_DONE"


class Data:
    def __init__(self, type, key, value):
        self.type = type
        self.key = key
        self.value = value

    def __repr__(self):
        return f"Data<{self.type}: {self.key}={self.value}>"


class ChainNode(Node):
    def __init__(self, id, left, right, central):
        super().__init__(str(id), self.loop)
        self.left = left
        self.right = right
        self.central = central
        self.data = {}
        self.waiting_for_ack = {}

    def loop(self):
        while True:
            msg = self.recv_any()
            if msg is None:
                time.sleep(0.05)
                continue
            data = msg.data
            if data.type == WRITE:
                if self.right is not None:
                    self.propagate_right(data)
                else:
                    self.waiting_for_ack[data.key] = data.value
                    self.propagate_left(Data(WRITE_DONE, data.key, data.value))
            elif data.type == WRITE_DONE:
                self.propagate_left(data)
            elif data.type == READ:
                self.log(f"Reading {data.key}={self.data.get(data.key)}")
                self.send_to(msg.by, Data(READ_DONE, data.key, self.data.get(data.key)))

    def propagate_right(self, data):
        self.log(f"Propagating right {data.key}:{data.value}")
        self.send_to(self.right, data)
        self.waiting_for_ack[data.key] = data.value

    def confirm(self, data):
        self.log(f"Starting confirm {data.key}:{data.value}")
        existing_value = self.waiting_for_ack[data.key]
        if existing_value is None:
            self.log(f"Error: no {data.key} waiting for ack")
        del self.waiting_for_ack[data.key]
        if existing_value != data.value:
            self.log(f"Error diffing values for key {data.key}: ack-{existing_value}, data-{data.value}")
        self.data[data.key] = data.value
        self.log(f"Confirmed {data.key}:{data.value}")

    def propagate_left(self, data):
        self.confirm(data)
        if self.left is not None:
            self.log(f"Confirming to node {self.left}")
            self.send_to(self.left, data)
        else:
            self.log(f"Confirming to central {self.central}")
            self.send_to(self.central, data)


class CentralNode(Node):
    def __init__(self, id, client, nodes):
        super().__init__(str(id), self.loop)
        self.nodes = nodes
        self.write = nodes[0]
        self.read = nodes[len(nodes) - 1]
        self.id = id
        self.client = client

    def loop(self):
        while True:
            msg = self.recv_any()
            if msg is None:
                time.sleep(0.05)
                continue
            data = msg.data
            if data.type == WRITE_DONE:
                self.log(f"Write finished {data.key}={data.value}")
                self.send_to(self.client, data)
            elif data.type == READ_DONE:
                self.log(f"Read finished {data.key}={data.value}")
                self.send_to(self.client, data)
            elif data.type == READ:
                self.log(f"Reading {data.key}")
                self.send_to(self.read, data)
            elif data.type == WRITE:
                self.log(f"Writing {data.key}={data.value}")
                self.send_to(self.write, data)
            else:
                self.log(f"Unknown request: {data}")


class Client(Node):
    def __init__(self, id, server):
        super().__init__(str(id), self.loop)
        self.server = server
        self.id = id

    def loop(self):
        self.send_to(self.server, Data(READ, 'no_value', None))
        msg = self.recv_block(self.server)
        self.log(f"Received {msg.data} (value should be none)")

        self.send_to(self.server, Data(WRITE, 'first', 123))
        self.send_to(self.server, Data(READ, 'first', None))
        msg = self.recv_block(self.server)
        self.log(f"Received {msg.data} (write_done/read_done and 123/read_done and None)")
        msg = self.recv_block(self.server)
        self.log(f"Received {msg.data} (write_done/read_done and 123/read_done and None)")
        time.sleep(0.5)
        self.send_to(self.server, Data(READ, 'first', None))
        msg = self.recv_block(self.server)
        self.log(f"Received {msg.data} read_done and 123")


def run():
    n = 5
    node_ids = [i for i in range(n)]
    node_names = [str(i) for i in node_ids]
    nodes = [ChainNode(id,
                       str(id - 1) if id > 0 else None,
                       str(id + 1) if id < len(node_ids) - 1 else None,
                       str("CENTRAL"))
             for id in node_ids] \
            + [CentralNode("CENTRAL", "CLIENT", node_names),
               Client("CLIENT", "CENTRAL")]
    links = [Link(str(i), str(i + 1)) for i in range(n - 1)] \
            + [Link("CENTRAL", str(i)) for i in node_ids] \
            + [Link("CENTRAL", "CLIENT")]

    network = Network(nodes, links)

    network.start()
    time.sleep(10)
    network.kill()
