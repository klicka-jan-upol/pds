from lib.sds import Link, Node, Network
from multiprocessing import current_process
import time
import math

size_of_address = 5
id_limit = math.pow(2, size_of_address)
REQUEST = 1
RESPONSE = 2


# Implementujte Chord systém z přednášky.
# Pro jednoduchost předpokládejte, že každý uzel systému má k dispozici seznam identifikátorů existujícícíh uzlů.

class IsWithinRange:
    def __init__(self, lower, key, upper):
        self.lower = lower
        self.key = key
        self.upper = upper
        self.normalize()

    def normalize(self):
        og_upper = self.upper
        if self.lower > self.upper:
            self.upper += id_limit
            if self.key < og_upper:
                self.key += id_limit

    def is_within_open_closed(self):
        return self.lower < self.key <= self.upper

    def is_within_open(self):
        return self.lower < self.key < self.upper


class ChordNode(Node):
    def __init__(self, id: int, all_nodes: []):
        super().__init__(f"{id}", self.loop)
        self.id = id
        self.predecessor = all_nodes[(all_nodes.index(id) - 1 + len(all_nodes) % len(all_nodes))]
        self.ft = [None for i in range(size_of_address)]
        self.all_nodes = all_nodes

    def loop(self):
        self.fill_ft()
        while True:
            msg = self.recv_any()
            if msg is not None:
                type = msg.data['type']
                key = int(msg.data['key'])
                who = msg.data['who']
                if type == REQUEST:
                    self.log(f"Received lookup request for {key}")
                    next_node = self.lookup(key)
                    if next_node == self.id:
                        self.send_to(who, {'key': key, 'node': self.id})
                    else:
                        self.send_to(str(next_node), msg.data)
                else:
                    print(f"{self.id}: Ignoring msg {msg}")

    def fill_ft(self):
        self.all_nodes.sort()
        for i in range(size_of_address):
            n = self.id
            m = size_of_address
            poww = int(math.pow(2, i))  # i - 1 when indexing from one
            mod = int(math.pow(2, m))
            key = int((n + poww) % mod)
            succ = self.key_successor(key)
            print(f"{n} FT: ({n} + {poww}) % {mod} = {key} -> succ({key}) = {succ}")
            self.ft[i] = succ

    def key_successor(self, k: int) -> int:
        # k <= id && no other node is k <= other_id && other_id < id
        for node in self.all_nodes:
            if k <= node:
                return node
        return self.all_nodes[0]

    def lookup(self, key):
        if self.key_belongs_to_me(key):
            self.log(f"Lookup finished for {key}!")
            return self.id
        if self.key_belongs_to_successor(key):
            self.log(f"Found successor {self.ft[0]} for key {key}")
            return self.ft[0]
        else:
            preced = self.closest_preceding(key)
            self.log(f"Found closes predecessor {preced} for {key}")
            return preced

    def key_belongs_to_me(self, key):
        range = IsWithinRange(self.predecessor, key, self.id)
        return range.is_within_open_closed()

    def key_belongs_to_successor(self, key):
        range = IsWithinRange(self.id, key, self.ft[0])
        return range.is_within_open_closed()

    def key_belongs_to_closest_predecessor(self, key, pred):
        range = IsWithinRange(self.id, pred, key)
        return range.is_within_open()

    def closest_preceding(self, key):
        for i in reversed(range(size_of_address)):
            if self.key_belongs_to_closest_predecessor(key, self.ft[i]):
                return self.ft[i]
        return self.id


def node_code(nodes_idx):
    node = current_process()
    node.log(f"Starting chord node {node.name}.")


def client_code():
    node = current_process()
    node.log(f"Starting client node.")

    time.sleep(1)

    # prvni příklad z přednášky
    node_start = "1"
    key = "26"
    node.send_to(node_start, {'who': node.name, 'key': key, 'type': REQUEST})

    # druhý příklad z přednášky
    node_start = "28"
    key = "12"
    node.send_to(node_start, {'who': node.name, 'key': key, 'type': REQUEST})

    msg = node.recv_any_block(10_000)
    node.log(f"Lookup finished for key: {msg.data['key']} and node: {msg.data['node']}")
    msg = node.recv_any_block(10_000)
    node.log(f"Lookup finished for key: {msg.data['key']} and node: {msg.data['node']}")


def run():
    nodes_ids = [1, 4, 9, 11, 14, 18, 20, 21, 28]

    nodes = [ChordNode(id, nodes_ids) for id in nodes_ids]
    links = [Link(str(i), str(j)) for j in nodes_ids for i in nodes_ids if i != j]

    client_node = [Node("client", client_code)]
    client_links = [Link("client", str(i)) for i in nodes_ids]

    network = Network(nodes + client_node, links + client_links)

    network.start()
    time.sleep(10)
    network.kill()
