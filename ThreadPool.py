import time
from multiprocessing.pool import ThreadPool
from threading import Thread

counter = 0


def add_in_thread() -> None:
    global counter
    local_counter = counter
    time.sleep(0.0001)
    local_counter += 1
    time.sleep(0.0001)
    counter = local_counter


def add_in_thread_arg(id) -> None:
    global counter
    local_counter = counter
    time.sleep(0.0001)
    local_counter += 1
    time.sleep(0.0001)
    counter = local_counter
    # print(counter)


# http://trnecka.inf.upol.cz/teaching/pds/cviceni01.html
def run_threads_naive(num_threads):
    global counter
    threads = []
    for x in range(num_threads):
        thread = Thread(target=add_in_thread)
        threads = threads + [thread]
        thread.start()
    for x in threads:
        x.join()
    print(counter)


# http://trnecka.inf.upol.cz/teaching/pds/cviceni02.html
def run_thread_pool(num_threads):
    global counter
    pool = ThreadPool(num_threads)
    pool.map(add_in_thread_arg, list(range(num_threads)))
    pool.close()
    pool.join()
    print(counter)


# Implementujte synchronizaci sdíleného čítače pomocí Lamportova Bakery algoritmu
# (předpokládejte, že operace max() není thread safe)
def awaitt(condition) -> None:
    while not condition():
        pass


number = [0, 0, 0, 0]
entering = [False, False, False, False]
maxx = 0


def max():
    global number
    max = number[0]
    for flag in number:
        if flag > max:
            max = flag
    return max

def bakery(i):
    print(f"started thread {i}")
    global number, entering, counter
    for t in range(50):
        entering[i] = True
        number[i] = 1 + max()
        entering[i] = False
        for y in range(len(number)):
            if y != i:
                awaitt(lambda: not entering[y])
                awaitt(lambda: number[y] == 0
                               or number[i] < number[y]
                               or number[i] == number[y] and i < y
                       )
        counter = counter + 1
        # print(f"incremented {i} to {counter} (iteration {t}")
        number[i] = False

def lamport_bakery():
    pool = ThreadPool(4)
    pool.map(bakery, list(range(4)))
    print(counter)
